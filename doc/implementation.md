
Implementation Notes
====================

Why Polymorphic Variants
------------------------

Using

```ocaml
type ('a, 'b) t =
  | Ok of 'a
  | Error of 'b
```

gives two annoying alternatives:

- be incompatible with `Core_kernel`'s `Result.t`, or
- depend on `Core_kernel`.

Type equality constraints do not work *twice*. I.e. one cannot write:

```ocaml
type ('a, 'b) result_core = ('a, 'b) Result.t  = Ok of 'a | Error of 'b
type ('a, 'b) result_err = ('a, 'b) Err.t  = Ok of 'a | Error of 'b
type ('a, 'b) result = ('a, 'b) result_err  = ('a, 'b) result_core
```

Polymorphic Variants are safe while not enforcing library dependencies.


Alternatives
------------

The following libraries use “regular/ordinary” variants and hence are
all incompatible from each other:

- Core's
[`Core.Std.Result`](https://ocaml.janestreet.com/ocaml-core/latest/doc/core_kernel/Result.html)
- Batteries'
[`BatResult`](http://ocaml-batteries-team.github.io/batteries-included/hdoc/BatResult.html)
- [ocaml-monad-exn](https://bitbucket.org/deplai_j/ocaml-monad-exn)


