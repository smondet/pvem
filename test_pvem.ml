

let test_compatibility () =
  let module Other_error = struct
    type ('a, 'b) result = [
      | `Ok of 'a
      | `Error of 'b
    ]
    let return x : (_, _) result = `Ok x
    let fail x : (_, _) result = `Error x
    let (>>=) x f : (_, _) result =
      match x with
      | `Ok o -> f o
      | `Error e -> `Error e
  end in
  let m1 =
    Other_error.(
      return 42
      >>= fun x ->
      return (x + 1)) in
  let m2 =
    Other_error.(
      return 42
      >>= fun x ->
      fail (`failure "bouh")) in
  let t1 =
    let open Pvem.Result in
    return 42
    >>= fun _ ->
    m1
  in
  let t2 =
    let open Pvem.Result in
    return 42
    >>= fun _ ->
    m2
  in
  let t3 =
    let open Pvem.Result in
    `Ok ()
    >>= fun () ->
    return ()
  in
  ignore (t1, t2, t3)


let test_compatibility_with_deferred () =
  let module Deferred = struct
    type 'a t = 'a
    let return x  = x
    let bind x f = f x
    let catch f on_exn =
      try f () with e -> on_exn e
  end in
  let module RD = Pvem.With_deferred(Deferred) in
  let t1 =
    let open RD in
    return 42
    >>= fun x ->
    fail (`bouh)
  in
  let t2 =
    let m1 = Deferred.(return (Pvem.Result.return ())) in
    let open RD in
    m1
    >>= fun () ->
    return 42
  in
  let t3 =
    let m2 = Deferred.(return (`Ok ())) in
    let open RD in
    m2
    >>= fun () ->
    return 42
  in
  ignore (t1, t2, t3)
